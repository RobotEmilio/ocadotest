package com.asuarezgalan.ocadotest.cluster.ui

import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel

class TestProductListFragment : ProductListFragment() {

    override fun injectMembers() {
        viewModel = testViewModel
    }

    companion object {
        lateinit var testViewModel: ClustersViewModel
    }
}