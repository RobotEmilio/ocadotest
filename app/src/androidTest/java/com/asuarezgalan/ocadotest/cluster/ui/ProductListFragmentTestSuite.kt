package com.asuarezgalan.ocadotest.cluster.ui

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.asuarezgalan.ocadotest.R
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import io.mockk.mockk
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith

@Ignore("I don't know why, but test keeps on crashing trying to inflate the XML")
@RunWith(AndroidJUnit4::class)
class ProductListFragmentTestSuite {

    private val clustersViewModel = mockk<ClustersViewModel>()

    @Test
    fun testFragment() {
        TestProductListFragment.testViewModel = clustersViewModel

        val scenario = launchFragmentInContainer<TestProductListFragment>()
        scenario.moveToState(Lifecycle.State.RESUMED)
        Espresso.onView(ViewMatchers.withId(R.id.product_list_container))
            .check { view, noViewFoundException ->
                assert(true) // Just testing fragment scenario framework
            }
    }

}