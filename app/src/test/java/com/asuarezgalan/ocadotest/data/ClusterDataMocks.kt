package com.asuarezgalan.ocadotest.data

import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkEntity
import com.asuarezgalan.ocadotest.cluster.data.network.ClustersNetworkEntity
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity

object ClusterDataMocks {

    fun getClusterNetworkEntity(): ClustersNetworkEntity = ClustersNetworkEntity(
        listOf(
            ClusterNetworkEntity(
                "Bananas",
                listOf(
                    ProductNetworkEntity(
                        id = 309396011,
                        price = 1.45f,
                        description = "Ocado Organic Fairtrade Bananas",
                        size = "6 per pack",
                        imageUrl = "https://mobile.ocado.com/webservices/catalogue/items/item/309396011/images/image/0/240x240.jpg"
                    )
                )
            )
        )
    )
}