package com.asuarezgalan.ocadotest.data

import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.product.data.ProductApi
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkSource
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class ProductNetworkSourceTests {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val productApi = mockk<ProductApi>()
    private lateinit var source: ProductNetworkSource

    @Before
    fun setUp() {
        source = ProductNetworkSource(productApi, testCoroutineRule.testDispatcherProvider)
    }

    @Test
    fun `Test network source when API returns successfully`() = testCoroutineRule.runBlockingTest {
        //Arrange
        val testId = 23
        coEvery { productApi.getProduct(any()) } returns ProductNetworkEntity(testId)

        //Act
        val result = source.retrieveProduct(testId)

        //Assert
        assert(result is Resource.Success && result.data == ProductNetworkEntity(testId))
    }

    @Test
    fun `Test network source when API crashes unexpectedly`() = testCoroutineRule.runBlockingTest {
        //Arrange
        val testId = 23
        coEvery { productApi.getProduct(any()) } throws IllegalStateException()

        //Act
        val result = source.retrieveProduct(testId)

        //Assert
        assert(result is Resource.Error && result.error is UnexpectedException)
    }


    @Test
    fun `Test network source when there is no connection`() = testCoroutineRule.runBlockingTest {
        //Arrange
        val testId = 23
        coEvery { productApi.getProduct(any()) } throws UnknownHostException()

        //Act
        val result = source.retrieveProduct(testId)

        //Assert
        assert(result is Resource.Error && result.error is NoNetworkException)
    }
}