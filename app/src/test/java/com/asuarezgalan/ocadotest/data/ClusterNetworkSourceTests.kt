package com.asuarezgalan.ocadotest.data

import com.asuarezgalan.ocadotest.cluster.data.ClusterApi
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkSource
import com.asuarezgalan.ocadotest.cluster.data.network.ClustersNetworkEntity
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.IllegalStateException
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class ClusterNetworkSourceTests {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val clusterApi = mockk<ClusterApi>()
    private lateinit var source : ClusterNetworkSource

    @Before
    fun setUp() {
        source = ClusterNetworkSource(clusterApi, testCoroutineRule.testDispatcherProvider)
    }

    @Test
    fun `Test network source when API returns successfully`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterApi.getClusters() } returns ClusterDataMocks.getClusterNetworkEntity()

        //Act
        val result = source.retrieveCluster()

        //Assert
        assert(result is Resource.Success && result.data == ClusterDataMocks.getClusterNetworkEntity().cluster)

    }

    @Test
    fun `Test network source when API crashes unexpectedly`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterApi.getClusters() } throws IllegalStateException()

        //Act
        val result = source.retrieveCluster()

        //Assert
        assert(result is Resource.Error && result.error is UnexpectedException)

    }

    @Test
    fun `Test network source when there is no connection`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterApi.getClusters() } throws UnknownHostException()

        //Act
        val result = source.retrieveCluster()

        //Assert
        assert(result is Resource.Error && result.error is NoNetworkException)

    }

}