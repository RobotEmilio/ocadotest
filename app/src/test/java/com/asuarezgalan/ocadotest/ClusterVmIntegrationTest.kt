package com.asuarezgalan.ocadotest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.asuarezgalan.ocadotest.cluster.data.ClusterApi
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkEntity
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkSource
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterRepositoryImpl
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterProductUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterTitleUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.data.ClusterDataMocks
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.IllegalStateException
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class ClusterVmIntegrationTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val mockClusterApi = mockk<ClusterApi>()
    private lateinit var clusterNetworkDs: ClusterNetworkSource
    private lateinit var clusterRepo: ClusterRepository
    private lateinit var clusterViewModel: ClustersViewModel
    private val clustersObserver = spyk<Observer<List<ClusterUi>>>()
    private val actionsObserver = spyk<Observer<UIAction>>()

    @Before
    fun setUp() {
        clusterNetworkDs = ClusterNetworkSource(mockClusterApi, testCoroutineRule.testDispatcherProvider)
        clusterRepo = ClusterRepositoryImpl(clusterNetworkDs)
        clusterViewModel = ClustersViewModel(clusterRepo, testCoroutineRule.testDispatcherProvider)
        clusterViewModel.getClustersLiveData().observeForever(clustersObserver)
        clusterViewModel.getUIActions().observeForever(actionsObserver)
    }

    @Test
    fun `Test flow when repository returns clusters successfully`() = testCoroutineRule.runBlockingTest {
        val testClusters =  ClusterDataMocks.getClusterNetworkEntity()
        coEvery { mockClusterApi.getClusters() } returns testClusters

        clusterViewModel.getClusters()

        //Assert
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        coVerify(exactly = 1) { clustersObserver.onChanged(match { matchData(it, testClusters.cluster) }) }
    }

    private fun matchData(
        uiClusters: List<ClusterUi>,
        testClusters: List<ClusterNetworkEntity>
    ): Boolean = testClusters.all { test ->
        uiClusters.any { it is ClusterTitleUi && it.name == test.tag} &&
        test.items.all { testProduct ->
            uiClusters.any { it is ClusterProductUi && it.productId == testProduct.id}
        }
    }

    @Test
    fun `Test flow when repository fails unexpectedly`() = testCoroutineRule.runBlockingTest {
        coEvery { mockClusterApi.getClusters() } throws IllegalStateException()

        clusterViewModel.getClusters()

        //Assert
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Error && it.exception is UnexpectedException
        }) }
    }

    @Test
    fun `Test flow when repository fails because of no internet`() = testCoroutineRule.runBlockingTest {
        coEvery { mockClusterApi.getClusters() } throws UnknownHostException()

        clusterViewModel.getClusters()

        //Assert
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        coVerify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Error && it.exception is NoNetworkException
        }) }
    }


}