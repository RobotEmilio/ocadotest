package com.asuarezgalan.ocadotest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.product.data.ProductApi
import com.asuarezgalan.ocadotest.product.data.ProductRepositoryImpl
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkSource
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import com.asuarezgalan.ocadotest.product.ui.model.ProductUi
import com.asuarezgalan.ocadotest.product.vm.ProductViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class ProductVmIntegrationTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val mockProductApi = mockk<ProductApi>()
    private lateinit var productNetworkDs: ProductNetworkSource
    private lateinit var productRepo: ProductRepository
    private lateinit var productViewModel: ProductViewModel
    private val productObserver = spyk<Observer<ProductUi>>()
    private val actionsObserver = spyk<Observer<UIAction>>()

    @Before
    fun setUp() {
        productNetworkDs =
            ProductNetworkSource(mockProductApi, testCoroutineRule.testDispatcherProvider)
        productRepo = ProductRepositoryImpl(productNetworkDs)
        productViewModel = ProductViewModel(productRepo, testCoroutineRule.testDispatcherProvider)
        productViewModel.getProductLiveData().observeForever(productObserver)
        productViewModel.getUIActions().observeForever(actionsObserver)
    }

    @Test
    fun `Test Product flow success`() = testCoroutineRule.runBlockingTest {
        val testInput = getTestProduct()
        coEvery { mockProductApi.getProduct(any()) } returns testInput

        productViewModel.getProduct(0) //Input doesn't matter

        //Assert
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && it.visible
            })
        }
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && !it.visible
            })
        }
        coVerify(exactly = 1) {
            productObserver.onChanged(match {
                it.name == testInput.title
                        && it.imageUrl == testInput.imageUrl
                        && it.price == testInput.price
                        && it.allergyInfo == testInput.allergyInformation
                        && it.description == testInput.description
            })
        }
    }

    private fun getTestProduct(): ProductNetworkEntity =
        ProductNetworkEntity(
            id = 81851011,
            price = 1f,
            title = "Ocado Shallots",
            imageUrl = "https://mobile.ocado.com/webservices/catalogue/items/item/81851011/images/image/0/360x360.jpg",
            description = "Our finely layered shallots have a bright flemish free skin and are milder and sweeter than onions",
            allergyInformation = "No allergens"
        )

    @Test
    fun `Test Product flow api breaks unexpectedly`() = testCoroutineRule.runBlockingTest {
        coEvery { mockProductApi.getProduct(any()) } throws IllegalStateException()

        productViewModel.getProduct(0) //Input doesn't matter

        //Assert
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && it.visible
            })
        }
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && !it.visible
            })
        }
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Error && it.exception is UnexpectedException
            })
        }
    }

    @Test
    fun `Test Product flow api breaks due to no internet`() = testCoroutineRule.runBlockingTest {
        coEvery { mockProductApi.getProduct(any()) } throws UnknownHostException()

        productViewModel.getProduct(0) //Input doesn't matter

        //Assert
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && it.visible
            })
        }
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Loading && !it.visible
            })
        }
        coVerify(exactly = 1) {
            actionsObserver.onChanged(match {
                it is UIAction.Error && it.exception is NoNetworkException
            })
        }
    }

}