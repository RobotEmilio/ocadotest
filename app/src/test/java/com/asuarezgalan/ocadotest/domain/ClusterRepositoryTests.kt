package com.asuarezgalan.ocadotest.domain

import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkSource
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterRepositoryImpl
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.data.ClusterDataMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ClusterRepositoryTests {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val clusterNetworkSource = mockk<ClusterNetworkSource>()
    private lateinit var clusterRepository : ClusterRepository

    @Before
    fun setUp() {
        clusterRepository = ClusterRepositoryImpl(clusterNetworkSource)
    }

    @Test
    fun `Test repository when network source returns successfully`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterNetworkSource.retrieveCluster() } returns Resource.Success(getDataCluster())

        //Act
        val result = clusterRepository.retrieveCluster()

        //Assert
        assert(result is Resource.Success && result.data == getDomainCluster())
    }

    @Test
    fun `Test repository when network source crashes`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterNetworkSource.retrieveCluster() } returns Resource.Error(UnexpectedException())

        //Act
        val result = clusterRepository.retrieveCluster()

        //Assert
        assert(result is Resource.Error && result.error is UnexpectedException)
    }

    private fun getDataCluster() =
        ClusterDataMocks.getClusterNetworkEntity().cluster

    private fun getDomainCluster() =
        listOf(ClusterDomainMocks.getCluster())
}