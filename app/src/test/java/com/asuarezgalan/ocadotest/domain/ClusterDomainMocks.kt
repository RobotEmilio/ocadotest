package com.asuarezgalan.ocadotest.domain

import com.asuarezgalan.ocadotest.cluster.domain.model.Cluster
import com.asuarezgalan.ocadotest.product.domain.model.Product

object ClusterDomainMocks {

    fun getCluster(): Cluster = Cluster(
        "Bananas",
        listOf(
            Product(
                id = 309396011,
                price = 1.45f,
                description = "Ocado Organic Fairtrade Bananas",
                size = "6 per pack",
                imageUrl = "https://mobile.ocado.com/webservices/catalogue/items/item/309396011/images/image/0/240x240.jpg"
            )
        )
    )
}