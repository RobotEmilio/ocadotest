package com.asuarezgalan.ocadotest.domain

import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.product.data.ProductRepositoryImpl
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkSource
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import com.asuarezgalan.ocadotest.product.domain.model.Product
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.IllegalStateException

@ExperimentalCoroutinesApi
class ProductRepositoryTests {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val productNetworkSource = mockk<ProductNetworkSource>()
    private lateinit var productRepository : ProductRepository

    @Before
    fun setUp() {
        productRepository = ProductRepositoryImpl(productNetworkSource)
    }

    @Test
    fun `Test repository when network source returns successfully`() = testCoroutineRule.runBlockingTest {
        val testId = 23
        coEvery { productNetworkSource.retrieveProduct(any()) } returns Resource.Success(ProductNetworkEntity(testId))

        val result = productRepository.retrieveProduct(testId)

        assert(result is Resource.Success && result.data == Product(id = testId))
    }

    @Test
    fun `Test repository when network source crashes`() = testCoroutineRule.runBlockingTest {
        val testId = 23
        coEvery { productNetworkSource.retrieveProduct(any()) } returns Resource.Error(UnexpectedException())

        val result = productRepository.retrieveProduct(testId)

        assert(result is Resource.Error && result.error is UnexpectedException)
    }
}