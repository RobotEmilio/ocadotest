package com.asuarezgalan.ocadotest.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import com.asuarezgalan.ocadotest.product.domain.model.Product
import com.asuarezgalan.ocadotest.product.ui.model.ProductUi
import com.asuarezgalan.ocadotest.product.vm.ProductViewModel
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ProductViewModelTestSuite {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val productRepository = mockk<ProductRepository>()
    private lateinit var viewModel: ProductViewModel
    private val productsObserver = spyk<Observer<ProductUi>>()
    private val actionsObserver = spyk<Observer<UIAction>>()

    @Before
    fun setUp() {
        viewModel = ProductViewModel(productRepository, testCoroutineRule.testDispatcherProvider)
        viewModel.getProductLiveData().observeForever(productsObserver)
        viewModel.getUIActions().observeForever(actionsObserver)
    }

    @Test
    fun `Test product retrieval success`() = testCoroutineRule.runBlockingTest {
        //Arrange
        val testProduct = getTestProduct()
        coEvery { productRepository.retrieveProduct(any()) } returns Resource.Success(testProduct)

        //Act
        viewModel.getProduct(0)

        //Assert
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        verify(exactly = 1) {
            productsObserver.onChanged(match {
                it.name == testProduct.title
                        && it.description == testProduct.description
                        && it.price == testProduct.price
                        && it.imageUrl == testProduct.imageUrl
            })
        }
    }

    @Test
    fun `Test product retrieval returns NoNetworkException`() = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { productRepository.retrieveProduct(any()) } returns Resource.Error(NoNetworkException())

        //Act
        viewModel.getProduct(0)

        //Assert
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Error && it.exception is NoNetworkException
        }) }
    }

    private fun getTestProduct() =
        Product(
            id = 81851011,
            price = 1f,
            title = "Ocado Shallots",
            imageUrl = "https://mobile.ocado.com/webservices/catalogue/items/item/81851011/images/image/0/360x360.jpg",
            description = "Our finely layered shallots have a bright flemish free skin and are milder and sweeter than onions",
            allergyInfo = "No allergens"
        )

}