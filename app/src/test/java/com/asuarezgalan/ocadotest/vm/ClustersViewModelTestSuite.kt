package com.asuarezgalan.ocadotest.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import com.asuarezgalan.ocadotest.cluster.vm.toClusterUi
import com.asuarezgalan.ocadotest.common.TestCoroutineRule
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.domain.ClusterDomainMocks
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ClustersViewModelTestSuite {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val clusterRepository = mockk<ClusterRepository>()
    private lateinit var viewmodel: ClustersViewModel
    private val clustersObserver = spyk<Observer<List<ClusterUi>>>()
    private val actionsObserver = spyk<Observer<UIAction>>()

    @Before
    fun setUp() {
        viewmodel = ClustersViewModel(clusterRepository, testCoroutineRule.testDispatcherProvider)
        viewmodel.getClustersLiveData().observeForever(clustersObserver)
        viewmodel.getUIActions().observeForever(actionsObserver)
    }

    @Test
    fun `Test cluster retrieval success`()  = testCoroutineRule.runBlockingTest {
        //Arrange
        val result = listOf(ClusterDomainMocks.getCluster())
        coEvery { clusterRepository.retrieveCluster() } returns Resource.Success(result)

        //Act
        viewmodel.getClusters()

        //Assert
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        coVerify(exactly = 1) { clustersObserver.onChanged(eq(result.toClusterUi())) }
    }

    @Test
    fun `Test cluster retrieval returns NoNetworkException`()  = testCoroutineRule.runBlockingTest {
        //Arrange
        coEvery { clusterRepository.retrieveCluster() } returns Resource.Error(NoNetworkException())

        //Act
        viewmodel.getClusters()

        //Assert
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Loading && !it.visible
        }) }
        verify(exactly = 1) { actionsObserver.onChanged(match {
            it is UIAction.Error && it.exception is NoNetworkException
        }) }
    }

}