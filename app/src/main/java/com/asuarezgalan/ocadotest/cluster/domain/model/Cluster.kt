package com.asuarezgalan.ocadotest.cluster.domain.model

import com.asuarezgalan.ocadotest.product.domain.model.Product

data class Cluster(
    val name: String,
    val items: List<Product>
)