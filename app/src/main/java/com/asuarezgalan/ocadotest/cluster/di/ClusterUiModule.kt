package com.asuarezgalan.ocadotest.cluster.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.asuarezgalan.ocadotest.cluster.ui.ProductListFragment
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import com.asuarezgalan.ocadotest.product.ui.ProductDetailFragment
import com.asuarezgalan.ocadotest.product.vm.ProductViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class ClusterUiModule {

    @ContributesAndroidInjector
    abstract fun contributesProductListFragment(): ProductListFragment

    @ContributesAndroidInjector
    abstract fun contributesProductDetailFragment(): ProductDetailFragment

    @Module
    class InjectViewModel {
        @Provides
        fun provideClusterViewModel(factory: ViewModelProvider.Factory, target: ProductListFragment) =
            ViewModelProviders.of(target, factory).get(ClustersViewModel::class.java)

        @Provides
        fun provideProductViewModel(factory: ViewModelProvider.Factory, target: ProductDetailFragment) =
            ViewModelProviders.of(target, factory).get(ProductViewModel::class.java)
    }
}