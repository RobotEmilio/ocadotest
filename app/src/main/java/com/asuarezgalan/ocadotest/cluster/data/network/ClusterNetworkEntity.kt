package com.asuarezgalan.ocadotest.cluster.data.network

import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import com.google.gson.annotations.SerializedName

/*
    SerializedName tags are put to make code obfuscation work
    Since id is the only identifier field, we made any other field nullable. This way we're forced to deal with possible null values
 */
data class ClustersNetworkEntity(
    @SerializedName("clusters") val cluster: List<ClusterNetworkEntity>
)

data class ClusterNetworkEntity(
    @SerializedName("tag") val tag: String,
    @SerializedName("items") val items: List<ProductNetworkEntity>
)