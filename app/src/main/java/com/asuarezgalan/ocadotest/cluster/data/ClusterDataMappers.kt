package com.asuarezgalan.ocadotest.cluster.data

import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkEntity
import com.asuarezgalan.ocadotest.cluster.domain.model.Cluster
import com.asuarezgalan.ocadotest.product.data.toProduct

fun ClusterNetworkEntity.toCluster() : Cluster {
    return Cluster(
        name = tag,
        items = items.map { it.toProduct() }
    )
}