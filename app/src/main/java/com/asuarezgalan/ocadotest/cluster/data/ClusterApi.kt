package com.asuarezgalan.ocadotest.cluster.data

import com.asuarezgalan.ocadotest.cluster.data.network.ClustersNetworkEntity
import retrofit2.http.GET

interface ClusterApi {

    @GET("products")
    suspend fun getClusters() : ClustersNetworkEntity
}