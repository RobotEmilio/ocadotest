package com.asuarezgalan.ocadotest.cluster.data.network

import com.asuarezgalan.ocadotest.cluster.data.ClusterApi
import com.asuarezgalan.ocadotest.common.DispatcherProvider
import com.asuarezgalan.ocadotest.common.data.NetworkSourceErrorHandler.handleNetworkSourceException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class ClusterNetworkSource @Inject constructor(
    private val api: ClusterApi,
    private val dispatcher: DispatcherProvider
) {

    suspend fun retrieveCluster(): Resource<List<ClusterNetworkEntity>> =
        try {
            val result = withContext(dispatcher.io()) {
                api.getClusters().cluster
            }

            Resource.Success(result)
        } catch (exception: Exception) {
            Resource.Error(handleNetworkSourceException(exception))
        }

}