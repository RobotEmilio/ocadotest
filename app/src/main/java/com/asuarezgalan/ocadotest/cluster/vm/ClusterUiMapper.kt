package com.asuarezgalan.ocadotest.cluster.vm

import com.asuarezgalan.ocadotest.cluster.domain.model.Cluster
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterProductUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterTitleUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi

fun List<Cluster>.toClusterUi(): List<ClusterUi> {
    val result: MutableList<ClusterUi> = mutableListOf()
    forEach { cluster ->
        result.add(ClusterTitleUi(cluster.name))
        cluster.items.forEach { product ->
            result.add(
                ClusterProductUi(
                    product.id,
                    product.title,
                    product.size,
                    product.price,
                    product.imageUrl
                )
            )
        }
    }
    return result
}