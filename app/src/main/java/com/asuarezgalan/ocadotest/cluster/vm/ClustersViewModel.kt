package com.asuarezgalan.ocadotest.cluster.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi
import com.asuarezgalan.ocadotest.common.BaseViewModel
import com.asuarezgalan.ocadotest.common.DispatcherProvider
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.common.ui.UIAction.Error
import com.asuarezgalan.ocadotest.common.ui.UIAction.Loading
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class ClustersViewModel @Inject constructor(
    private val clusterRepo: ClusterRepository,
    private val dispatcher: DispatcherProvider
) : BaseViewModel() {

    private val clusters: MutableLiveData<List<ClusterUi>> = MutableLiveData()
    private val actions: LiveEvent<UIAction> = LiveEvent()

    fun getClusters() {
        viewModelScope.launch(dispatcher.main()) {
            actions.value = Loading(true)

            val resource = async(dispatcher.default()) {
                clusterRepo.retrieveCluster()
            }

            val cluster = resource.await()
            actions.value = Loading(false)
            when(cluster) {
                is Resource.Success -> clusters.value = cluster.data.toClusterUi()
                is Resource.Error -> actions.value = Error(cluster.error)
            }
        }
    }

    fun getClustersLiveData(): LiveData<List<ClusterUi>> = clusters
    fun getUIActions(): LiveData<UIAction> = actions

}