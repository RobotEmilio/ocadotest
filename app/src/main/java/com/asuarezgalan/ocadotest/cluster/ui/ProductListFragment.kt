package com.asuarezgalan.ocadotest.cluster.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.asuarezgalan.ocadotest.R
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import com.asuarezgalan.ocadotest.common.BaseFragment
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.getViewModel
import com.asuarezgalan.ocadotest.common.observe
import com.asuarezgalan.ocadotest.common.showToast
import com.asuarezgalan.ocadotest.common.ui.UIAction
import kotlinx.android.synthetic.main.fragment_product_list.*
import javax.inject.Inject

open class ProductListFragment : BaseFragment() {

    @Inject
    lateinit var viewModel : ClustersViewModel

    private val clustersAdapter = ClustersAdapter { onProductClicked(it) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setViews()
        observeViewModel()
        viewModel.getClusters()
    }

    private fun observeViewModel() {
        observe(viewModel.getClustersLiveData(), { handleClusterResource(it) })
        observe(viewModel.getUIActions(), { handleActions(it) })
    }

    private fun handleActions(action: UIAction?) {
        when (action) {
            is UIAction.Loading -> product_list_swipe_refresh.isRefreshing = action.visible
            is UIAction.Error -> handleError(action.exception)
        }
    }

    private fun handleError(exception: Exception) {
        when (exception) {
            is UnexpectedException -> getString(R.string.error_unexpected).showToast(context)
            is NoNetworkException -> getString(R.string.error_no_connection).showToast(context)
        }
    }

    private fun handleClusterResource(clusters: List<ClusterUi>?) {
        clusters?.let { clustersAdapter.setClustersData(it) }
    }

    private fun setViews() {
        setSwipeRefresh()
        setRecyclerView()
    }

    private fun setSwipeRefresh() {
        product_list_swipe_refresh.setOnRefreshListener {
            viewModel.getClusters()
        }
    }

    private fun setRecyclerView() {
        with(product_list_container) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = clustersAdapter
        }
    }

    private fun onProductClicked(id: Int) {
        findNavController().navigate(ProductListFragmentDirections.goToDetail(id))
    }

}
