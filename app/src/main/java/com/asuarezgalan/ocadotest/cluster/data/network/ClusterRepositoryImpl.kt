package com.asuarezgalan.ocadotest.cluster.data.network

import com.asuarezgalan.ocadotest.cluster.data.toCluster
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import com.asuarezgalan.ocadotest.cluster.domain.model.Cluster
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.data.model.handleResource
import javax.inject.Inject

class ClusterRepositoryImpl @Inject constructor(
    private val networkDs: ClusterNetworkSource
): ClusterRepository {

    override suspend fun retrieveCluster(): Resource<List<Cluster>> =
        networkDs.retrieveCluster().handleResource { res ->
            res.map { it.toCluster() }
        }

}