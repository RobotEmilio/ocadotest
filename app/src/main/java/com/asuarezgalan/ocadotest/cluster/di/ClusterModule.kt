package com.asuarezgalan.ocadotest.cluster.di

import com.asuarezgalan.ocadotest.cluster.data.ClusterApi
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterNetworkSource
import com.asuarezgalan.ocadotest.cluster.data.network.ClusterRepositoryImpl
import com.asuarezgalan.ocadotest.cluster.domain.ClusterRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ClusterModule {

    @Provides
    fun prodiveClusterApi(retrofit: Retrofit) : ClusterApi = retrofit.create(ClusterApi::class.java)

    @Provides
    fun provideClusterRepository(networkDs: ClusterNetworkSource) : ClusterRepository =
        ClusterRepositoryImpl(networkDs)
}