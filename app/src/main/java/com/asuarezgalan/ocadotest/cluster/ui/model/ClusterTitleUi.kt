package com.asuarezgalan.ocadotest.cluster.ui.model

data class ClusterTitleUi(
    val name: String
) : ClusterUi {

    override fun getViewType(): ClusterUi.Type = ClusterUi.Type.TITLE

}