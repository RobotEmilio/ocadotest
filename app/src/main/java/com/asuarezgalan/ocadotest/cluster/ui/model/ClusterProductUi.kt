package com.asuarezgalan.ocadotest.cluster.ui.model

data class ClusterProductUi(
    val productId: Int,
    val name: String? = null,
    val size: String? = null,
    val price: Float? = null,
    val imageUrl: String? = null
) : ClusterUi {

    override fun getViewType(): ClusterUi.Type = ClusterUi.Type.ITEM
}