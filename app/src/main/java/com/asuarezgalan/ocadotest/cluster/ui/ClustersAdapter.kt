package com.asuarezgalan.ocadotest.cluster.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.asuarezgalan.ocadotest.R
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterProductUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterTitleUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi.Type.ITEM
import com.asuarezgalan.ocadotest.cluster.ui.model.ClusterUi.Type.TITLE

class ClustersAdapter(private val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var data: MutableList<ClusterUi> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (ClusterUi.Type.fromInt(viewType)) {
            TITLE -> TitleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_list_title, parent, false))
            ITEM -> ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_list_item, parent, false))
            else -> throw IllegalStateException("Unknown cluster type")
        }
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (ClusterUi.Type.fromInt(getItemViewType(position))) {
            TITLE -> (holder as TitleViewHolder).bind(data[position] as ClusterTitleUi)
            ITEM -> (holder as ProductViewHolder).bind(data[position] as ClusterProductUi)
            else -> throw IllegalStateException("Unknown cluster type")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].getViewType().value
    }

    inner class TitleViewHolder(private val view: View) :
        RecyclerView.ViewHolder(view) {
        fun bind(title: ClusterTitleUi) {
            view.findViewById<AppCompatTextView>(R.id.cluster_name).text = title.name
        }
    }

    inner class ProductViewHolder(private val view: View) :
        RecyclerView.ViewHolder(view) {
        fun bind(item: ClusterProductUi) {
            view.findViewById<AppCompatImageView>(R.id.item_image).load(item.imageUrl) {
                crossfade(true)
                placeholder(R.drawable.ic_apple)
            }
            view.findViewById<AppCompatTextView>(R.id.item_title).text = item.name
            view.findViewById<AppCompatTextView>(R.id.item_subtitle).text = item.size
            view.findViewById<AppCompatTextView>(R.id.item_price).text = "£${"%.2f".format(item.price)}"
            view.setOnClickListener {
                onClick(item.productId)
            }
        }
    }

    fun setClustersData(newData: List<ClusterUi>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

}