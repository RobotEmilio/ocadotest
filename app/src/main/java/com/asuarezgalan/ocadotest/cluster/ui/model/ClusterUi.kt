package com.asuarezgalan.ocadotest.cluster.ui.model

interface ClusterUi {

    enum class Type(val value: Int) {
        TITLE(0), ITEM(1);

        companion object {
            private val map = values().associateBy(Type::value)
            fun fromInt(type: Int) = map[type]
        }
    }

    fun getViewType(): Type
}