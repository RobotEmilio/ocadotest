package com.asuarezgalan.ocadotest.cluster.domain

import com.asuarezgalan.ocadotest.cluster.domain.model.Cluster
import com.asuarezgalan.ocadotest.common.data.model.Resource

interface ClusterRepository {
    suspend fun retrieveCluster(): Resource<List<Cluster>>
}