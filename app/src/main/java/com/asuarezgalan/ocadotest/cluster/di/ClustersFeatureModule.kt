package com.asuarezgalan.ocadotest.cluster.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.asuarezgalan.ocadotest.cluster.ui.ProductListFragment
import com.asuarezgalan.ocadotest.cluster.vm.ClustersViewModel
import com.asuarezgalan.ocadotest.di.ActivityScope
import com.asuarezgalan.ocadotest.di.vm.ViewModelKey
import com.asuarezgalan.ocadotest.main.ui.MainActivity
import com.asuarezgalan.ocadotest.product.di.ProductModule
import com.asuarezgalan.ocadotest.product.vm.ProductViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ProductModule::class, ClusterModule::class])
abstract class ClustersFeatureModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [ClusterUiModule::class])
    abstract fun contributeMainActivity() : MainActivity

    @Binds
    @IntoMap
    @ViewModelKey(ClustersViewModel::class)
    abstract fun bindClustersViewModel(clustersViewModel: ClustersViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductViewModel::class)
    abstract fun bindProductViewModell(productViewModel: ProductViewModel) : ViewModel

}