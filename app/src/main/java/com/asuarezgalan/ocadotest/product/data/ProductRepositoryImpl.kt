package com.asuarezgalan.ocadotest.product.data

import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.data.model.handleResource
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkSource
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import com.asuarezgalan.ocadotest.product.domain.model.Product
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val networkDataSource : ProductNetworkSource
) : ProductRepository {

    override suspend fun retrieveProduct(testId: Int): Resource<Product> =
        networkDataSource.retrieveProduct(testId).handleResource { it.toProduct() }

}