package com.asuarezgalan.ocadotest.product.data.network

import com.google.gson.annotations.SerializedName

/*
    SerializedName tags are put to make code obfuscation work
    Since id is the only identifier field, we made any other field nullable. This way we're forced to deal with possible null values
 */
data class ProductNetworkEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("price") val price: Float? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("imageUrl") val imageUrl: String? = null,
    @SerializedName("size") val size: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("allergyInformation") val allergyInformation: String? = null
)