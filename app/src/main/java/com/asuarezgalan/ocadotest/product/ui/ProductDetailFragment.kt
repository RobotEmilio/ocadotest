package com.asuarezgalan.ocadotest.product.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.asuarezgalan.ocadotest.R
import com.asuarezgalan.ocadotest.common.BaseFragment
import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import com.asuarezgalan.ocadotest.common.observe
import com.asuarezgalan.ocadotest.common.showToast
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.product.ui.model.ProductUi
import com.asuarezgalan.ocadotest.product.vm.ProductViewModel
import kotlinx.android.synthetic.main.fragment_product_detail.*
import javax.inject.Inject

class ProductDetailFragment : BaseFragment() {

    @Inject
    lateinit var viewModel : ProductViewModel

    private val arguments : ProductDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViews()
        observeViewModel()

        viewModel.getProduct(arguments.productId)
    }

    private fun setViews() {
        setToolbar()
    }

    private fun setToolbar() {
        with(product_detail_toolbar) {
            setNavigationIcon(R.drawable.ic_back)
            setNavigationOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

    private fun observeViewModel() {
        observe(viewModel.getProductLiveData(), ::handleProduct)
        observe(viewModel.getUIActions(), ::handleActions)
    }

    private fun handleProduct(product: ProductUi?) {
        product?.let { uiProduct ->
            product_image.load(uiProduct.imageUrl)
            uiProduct.name?.let { product_name.text = it }
            uiProduct.description?.let { product_description.text = it }
            uiProduct.allergyInfo?.let { product_allergens.text = it }
            uiProduct.price?.let { product_price.text = "£${"%.2f".format(it)}" }
        }
    }

    private fun handleActions(action: UIAction?) {
        when (action) {
            is UIAction.Error -> handleError(action.exception)
        }
    }

    private fun handleError(exception: Exception) {
        when (exception) {
            is UnexpectedException -> getString(R.string.error_unexpected).showToast(context)
            is NoNetworkException -> getString(R.string.error_no_connection).showToast(context)
        }
    }

}