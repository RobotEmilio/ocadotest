package com.asuarezgalan.ocadotest.product.data.network

import com.asuarezgalan.ocadotest.common.DispatcherProvider
import com.asuarezgalan.ocadotest.common.data.NetworkSourceErrorHandler.handleNetworkSourceException
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.product.data.ProductApi
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProductNetworkSource @Inject constructor(
    private val api: ProductApi,
    private val dispatcher: DispatcherProvider
) {

    suspend fun retrieveProduct(id: Int): Resource<ProductNetworkEntity> =
        try {
            //Since communicating with API is IO stuff, we change to IO Dispatcher
            val result = withContext(dispatcher.io()) {
                api.getProduct(id)
            }

            Resource.Success(result)
        } catch (exception: Exception) {
            Resource.Error(handleNetworkSourceException(exception))
        }

}