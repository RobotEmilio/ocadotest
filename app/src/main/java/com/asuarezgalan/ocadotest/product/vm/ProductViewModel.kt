package com.asuarezgalan.ocadotest.product.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.asuarezgalan.ocadotest.common.BaseViewModel
import com.asuarezgalan.ocadotest.common.DispatcherProvider
import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.common.ui.UIAction
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import com.asuarezgalan.ocadotest.product.domain.model.Product
import com.asuarezgalan.ocadotest.product.ui.model.ProductUi
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProductViewModel @Inject constructor(
    private val productRepo: ProductRepository,
    private val dispatcher: DispatcherProvider
) : BaseViewModel() {

    private val productLiveData: MutableLiveData<ProductUi> = MutableLiveData()
    private val actions: LiveEvent<UIAction> = LiveEvent()

    fun getProduct(id: Int) {
        viewModelScope.launch(dispatcher.main()) {
            actions.value = UIAction.Loading(true)
            val resource = async {
                productRepo.retrieveProduct(id)
            }

            val product = resource.await()
            actions.value = UIAction.Loading(false)
            when(product) {
                is Resource.Success -> productLiveData.value = product.data.toProductUi()
                is Resource.Error -> actions.value = UIAction.Error(product.error)
            }
        }
    }

    fun getProductLiveData(): LiveData<ProductUi> = productLiveData
    fun getUIActions(): LiveData<UIAction> = actions

}