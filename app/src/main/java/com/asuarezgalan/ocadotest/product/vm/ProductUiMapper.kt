package com.asuarezgalan.ocadotest.product.vm

import com.asuarezgalan.ocadotest.product.domain.model.Product
import com.asuarezgalan.ocadotest.product.ui.model.ProductUi

fun Product.toProductUi() : ProductUi {
    return ProductUi(
        name = title,
        imageUrl = imageUrl,
        description = description,
        allergyInfo = allergyInfo,
        price = price
    )
}