package com.asuarezgalan.ocadotest.product.data

import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductApi {

    @GET("product/{product_id}/")
    suspend fun getProduct(@Path("product_id") id: Int): ProductNetworkEntity
}