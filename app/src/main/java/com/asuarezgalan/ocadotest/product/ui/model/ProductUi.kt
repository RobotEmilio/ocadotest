package com.asuarezgalan.ocadotest.product.ui.model

data class ProductUi(
    val name: String? = null,
    val imageUrl: String? = null,
    val price: Float? = null,
    val description: String? = null,
    val allergyInfo: String? = null
)