package com.asuarezgalan.ocadotest.product.domain

import com.asuarezgalan.ocadotest.common.data.model.Resource
import com.asuarezgalan.ocadotest.product.domain.model.Product

interface ProductRepository {
    suspend fun retrieveProduct(testId: Int): Resource<Product>
}