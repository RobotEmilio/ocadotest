package com.asuarezgalan.ocadotest.product.domain.model

data class Product(
    val id: Int,
    val price: Float? = null,
    val title: String? = null,
    val size: String? = null,
    val imageUrl: String? = null,
    val description: String? = null,
    val allergyInfo: String? = null
) {

    override fun equals(other: Any?): Boolean {
        return other is Product && other.id == id
    }
}