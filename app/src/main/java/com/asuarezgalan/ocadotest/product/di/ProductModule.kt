package com.asuarezgalan.ocadotest.product.di

import com.asuarezgalan.ocadotest.product.data.ProductApi
import com.asuarezgalan.ocadotest.product.data.ProductRepositoryImpl
import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkSource
import com.asuarezgalan.ocadotest.product.domain.ProductRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ProductModule {

    @Provides
    fun provideProductApi(retrofit: Retrofit) : ProductApi = retrofit.create(ProductApi::class.java)

    @Provides
    fun provideProductRepository(networkSource: ProductNetworkSource) : ProductRepository =
        ProductRepositoryImpl(networkSource)

}