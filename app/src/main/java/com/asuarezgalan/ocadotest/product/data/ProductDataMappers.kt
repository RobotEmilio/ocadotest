package com.asuarezgalan.ocadotest.product.data

import com.asuarezgalan.ocadotest.product.data.network.ProductNetworkEntity
import com.asuarezgalan.ocadotest.product.domain.model.Product

fun ProductNetworkEntity.toProduct() : Product {
    return Product(
        id = id,
        price = price,
        title = title,
        size = size,
        imageUrl = imageUrl,
        description = description,
        allergyInfo = allergyInformation
    )
}