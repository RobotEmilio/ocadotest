package com.asuarezgalan.ocadotest.common.data

import com.asuarezgalan.ocadotest.common.data.exceptions.NoNetworkException
import com.asuarezgalan.ocadotest.common.data.exceptions.UnexpectedException
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object NetworkSourceErrorHandler {

    fun handleNetworkSourceException(exception: Exception): Exception {
        if (exception is UnknownHostException || exception is SocketTimeoutException) return NoNetworkException(exception)
        return UnexpectedException(exception)
    }

}