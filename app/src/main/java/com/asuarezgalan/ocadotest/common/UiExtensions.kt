package com.asuarezgalan.ocadotest.common

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.*
import dagger.android.support.DaggerAppCompatActivity

inline fun <reified T : ViewModel> BaseFragment.getViewModel(
    viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(
    liveData: L,
    body: (T?) -> Unit) {
    liveData.observe(this, Observer(body))
}

fun String.showToast(context: Context?) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}