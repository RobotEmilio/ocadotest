package com.asuarezgalan.ocadotest.common.data.exceptions

import java.lang.Exception

class NoNetworkException(exception: Exception) : Exception(exception) {
    constructor() : this(Exception())
}