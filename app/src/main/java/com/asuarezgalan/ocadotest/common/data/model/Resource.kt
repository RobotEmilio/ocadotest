package com.asuarezgalan.ocadotest.common.data.model

import java.lang.Exception

/**
 * Class used for asking models to external sources
 *
 * https://developer.android.com/jetpack/docs/guide?hl=es-419#addendum
 */
sealed class Resource<T> {
    class Success<T>(val data: T) : Resource<T>()
    class Error<T>(val error: Exception) : Resource<T>()
}

/*
 * This method exists in order to handle model changes
 *
 * If no onLoading or onError lambda method is specified it just propagates previous value
 */
fun <T, U> Resource<T>.handleResource(
    onError: ((Exception) -> Exception)? = null,
    onSuccess: (T) -> U
): Resource<U> {
    return when (this) {
        is Resource.Success -> {
            Resource.Success(onSuccess(data))
        }
        is Resource.Error -> {
            Resource.Error(onError?.invoke(error) ?: error)
        }
    }
}