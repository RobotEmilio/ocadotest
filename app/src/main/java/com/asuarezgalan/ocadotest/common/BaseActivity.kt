package com.asuarezgalan.ocadotest.common

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()