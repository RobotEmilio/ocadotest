package com.asuarezgalan.ocadotest.common.ui

open class UIAction {
    class Loading(val visible: Boolean) : UIAction()
    class Error(val exception: Exception) : UIAction()
}