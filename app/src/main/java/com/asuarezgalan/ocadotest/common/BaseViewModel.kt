package com.asuarezgalan.ocadotest.common

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()