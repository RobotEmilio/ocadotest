package com.asuarezgalan.ocadotest.common

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
 * Apparently, there's a bug in runBlockingTest that makes tests fail if the executed code creates another coroutine
 * So for the time being we need to inject the dispatchers in the code.
 *
 * More info: https://craigrussell.io/2019/11/unit-testing-coroutine-suspend-functions-using-testcoroutinedispatcher/
 */

interface DispatcherProvider {
    fun main() : CoroutineDispatcher = Dispatchers.Main
    fun io() : CoroutineDispatcher = Dispatchers.IO
    fun default() : CoroutineDispatcher = Dispatchers.Default
    fun unconfined() : CoroutineDispatcher = Dispatchers.Unconfined
}

class DefaultDispatcherProvider : DispatcherProvider