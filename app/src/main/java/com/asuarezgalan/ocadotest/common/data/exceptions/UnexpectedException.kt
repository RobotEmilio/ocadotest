package com.asuarezgalan.ocadotest.common.data.exceptions

import java.lang.Exception

class UnexpectedException(exception: Exception) : Exception(exception) {
    constructor() : this(Exception())
}