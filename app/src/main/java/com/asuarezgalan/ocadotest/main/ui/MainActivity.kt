package com.asuarezgalan.ocadotest.main.ui

import android.os.Bundle
import com.asuarezgalan.ocadotest.R
import com.asuarezgalan.ocadotest.common.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
