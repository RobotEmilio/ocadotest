package com.asuarezgalan.ocadotest

import com.asuarezgalan.ocadotest.di.DaggerBaseApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class BaseApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerBaseApplicationComponent.builder().create(this)


}