package com.asuarezgalan.ocadotest.di

import androidx.lifecycle.ViewModelProvider
import com.asuarezgalan.ocadotest.di.vm.ViewModelFactoryProvider
import com.asuarezgalan.ocadotest.cluster.di.ClustersFeatureModule
import dagger.Binds
import dagger.Module

@Module(includes = [ClustersFeatureModule::class])
abstract class FeaturesModule {

    @Binds
    abstract fun bindViewModelFactory(provider: ViewModelFactoryProvider) : ViewModelProvider.Factory

}