package com.asuarezgalan.ocadotest.di

import android.content.Context
import com.asuarezgalan.ocadotest.BaseApplication
import com.asuarezgalan.ocadotest.common.DefaultDispatcherProvider
import com.asuarezgalan.ocadotest.common.DispatcherProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BaseModule {

    @Singleton
    @Provides
    fun provideApplicationContext(application: BaseApplication): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideDispatcherProvider() : DispatcherProvider = DefaultDispatcherProvider()

}