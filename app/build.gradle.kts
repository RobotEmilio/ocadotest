import com.asuarezgalan.gradle.Libs
import com.asuarezgalan.gradle.TestingLibs
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "com.asuarezgalan.baseproject"
        minSdkVersion(24)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro")
        }
    }

    (kotlinOptions as KotlinJvmOptions).apply {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(Libs.Kotlin)
    implementation(Libs.LiveEvent)

    networkAndApiDependencies()
    dependencyInjectionDependencies()
    androidDependencies()
    imageLoadingDependencies()

    testDependencies()
    androidTestDependencies()
}

fun DependencyHandlerScope.imageLoadingDependencies() {
    implementation(Libs.Coil)
}

fun DependencyHandlerScope.networkAndApiDependencies() {
    implementation(Libs.Retrofit.Base)
    implementation(Libs.Retrofit.GsonConverter)
    implementation(Libs.Retrofit.HttpLoggingInterceptor)
}

fun DependencyHandlerScope.androidDependencies() {
    implementation(Libs.Android.LifecycleExtensions)
    implementation(Libs.Android.Core)
    implementation(Libs.Android.AppCompat)
    implementation(Libs.Android.ConstraintLayout)
    implementation(Libs.Android.NavigationFragment)
    implementation(Libs.Android.NavigationUi)
}

fun DependencyHandlerScope.dependencyInjectionDependencies() {
    implementation(Libs.Dagger.Dagger)
    kapt(Libs.Dagger.DaggerCompiler)
    implementation(Libs.Dagger.DaggerAndroid)
    kapt(Libs.Dagger.DaggerAndroidProcessor)
    implementation(Libs.Dagger.DaggerSupport)
}

fun DependencyHandlerScope.testDependencies() {
    testImplementation(TestingLibs.CoroutinesTest)
    testImplementation(TestingLibs.JUnit)
    testImplementation(TestingLibs.MockK)
    testImplementation(TestingLibs.ArchTesting)
}

fun DependencyHandlerScope.androidTestDependencies() {
    androidTestImplementation(TestingLibs.JUnit)
    androidTestImplementation(TestingLibs.MockKAndroid)
    androidTestImplementation(TestingLibs.Android.TestRunner)
    androidTestImplementation(TestingLibs.Android.Espresso)
    androidTestImplementation(TestingLibs.Android.Core)
    androidTestImplementation(TestingLibs.Android.Rules)
    androidTestImplementation(TestingLibs.Android.Truth)
    androidTestImplementation(TestingLibs.Android.Monitor)
    debugImplementation(TestingLibs.Android.FragmentTesting) {
        exclude("androidx.test", "core")
    }
}
