package com.asuarezgalan.gradle

object Versions {

    const val Kotlin = "1.4.21"
    const val LiveEvent = "1.0.1"
    const val Retrofit = "2.9.0"
    const val HttpLoggingInterceptor = "4.9.0"
    const val Dagger = "2.30.1"
    const val Coil = "1.1.0"

    object Android {
        const val Navigation = "2.1.0"
        const val ConstraintLayout = "2.0.4"
        const val Lifecycle = "2.2.0"
        const val Core = "1.3.2"
        const val AppCompat = "1.2.0"
        const val NavigationSafeArgs = "2.3.2"
    }
}