package com.asuarezgalan.gradle

object TestingLibs {

    const val JUnit = "androidx.test.ext:junit:${TestingVersions.JUnit}"
    const val MockK = "io.mockk:mockk:${TestingVersions.MockK}"
    const val MockKAndroid = "io.mockk:mockk-android:${TestingVersions.MockK}"
    const val CoroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${TestingVersions.CoroutinesTest}"
    const val ArchTesting = "android.arch.core:core-testing:${TestingVersions.Android.ArchTesting}"

    object Android {
        const val Core = "androidx.test:core:${TestingVersions.Android.Core}"
        const val Rules = "androidx.test:rules:${TestingVersions.Android.Rules}"
        const val Truth = "androidx.test.ext:truth:${TestingVersions.Android.Truth}"
        const val Monitor = "androidx.test:monitor:${TestingVersions.Android.Monitor}"
        const val FragmentTesting = "androidx.fragment:fragment-testing:${TestingVersions.Android.FragmentTesting}"
        const val TestRunner = "androidx.test:runner:${TestingVersions.Android.TestRunner}"
        const val Espresso = "androidx.test.espresso:espresso-core:${TestingVersions.Android.Espresso}"
    }

}