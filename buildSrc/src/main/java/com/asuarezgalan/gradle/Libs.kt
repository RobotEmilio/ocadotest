package com.asuarezgalan.gradle

object Libs {
    const val Kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.Kotlin}"

    const val LiveEvent = "com.github.hadilq.liveevent:liveevent:${Versions.LiveEvent}"

    object Android {
        const val LifecycleExtensions =
            "androidx.lifecycle:lifecycle-extensions:${Versions.Android.Lifecycle}"
        const val Core = "androidx.core:core-ktx:${Versions.Android.Core}"
        const val AppCompat = "androidx.appcompat:appcompat:${Versions.Android.AppCompat}"
        const val ConstraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.Android.ConstraintLayout}"
        const val NavigationFragment =
            "androidx.navigation:navigation-fragment-ktx:${Versions.Android.Navigation}"
        const val NavigationUi =
            "androidx.navigation:navigation-ui-ktx:${Versions.Android.Navigation}"
        const val NavigationSafeArgs =
            "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.Android.NavigationSafeArgs}"
    }

    object Retrofit {
        const val Base = "com.squareup.retrofit2:retrofit:${Versions.Retrofit}"
        const val GsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.Retrofit}"
        const val HttpLoggingInterceptor =
            "com.squareup.okhttp3:logging-interceptor:${Versions.HttpLoggingInterceptor}"
    }

    object Dagger {
        const val Dagger = "com.google.dagger:dagger:${Versions.Dagger}"
        const val DaggerCompiler = "com.google.dagger:dagger-compiler:${Versions.Dagger}"
        const val DaggerAndroid = "com.google.dagger:dagger-android:${Versions.Dagger}"
        const val DaggerAndroidProcessor =
            "com.google.dagger:dagger-android-processor:${Versions.Dagger}"
        const val DaggerSupport = "com.google.dagger:dagger-android-support:${Versions.Dagger}"
    }

    const val Coil = "io.coil-kt:coil:${Versions.Coil}"
}