package com.asuarezgalan.gradle

object TestingVersions {
    const val JUnit = "1.1.0"
    const val MockK = "1.9.3"
    const val CoroutinesTest = "1.3.2"

    object Android {
        const val TestRunner = "1.1.1"
        const val Espresso = "3.1.1"
        const val FragmentTesting = "1.1.0"
        const val Rules = "1.1.1"
        const val Monitor = "1.1.0"
        const val Core = "1.1.0"
        const val Truth = "1.1.0"
        const val ArchTesting = "1.1.1"
    }
}